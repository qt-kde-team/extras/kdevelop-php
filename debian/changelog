kdevelop-php (24.12.1-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (24.12.1).
  * Build against Qt6, update build-deps and deps with the info from cmake.
  * Bump Standards-Version to 4.7.0, no change required.
  * Added myself to the uploaders.
  * Update the list of installed files.
  * Build with hardening=+all build hardening flag.

 -- Aurélien COUDERC <coucouf@debian.org>  Mon, 20 Jan 2025 21:51:18 +0100

kdevelop-php (23.08.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump the kdevelop-dev build dependency to 23.08.1, as indicated by the
    upstream build system.
  * Bump KDEV_PLUGIN_VERSION to 512, according to upstream.
  * Remove an obsolete maintscript entry.

 -- Pino Toscano <pino@debian.org>  Sun, 24 Sep 2023 17:28:30 +0200

kdevelop-php (22.12.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump the kdevelop-dev build dependency to 22.12.2, as indicated by the
    upstream build system.

 -- Pino Toscano <pino@debian.org>  Sun, 05 Feb 2023 12:54:57 +0100

kdevelop-php (22.12.0-1) unstable; urgency=medium

  * New upstream release.
  * Update standards version to 4.6.2, no changes needed.
  * Bump the kdevelop-dev build dependency to 22.12.0, as indicated by the
    upstream build system.
  * Bump KDEV_PLUGIN_VERSION to 510, according to upstream.

 -- Pino Toscano <pino@debian.org>  Thu, 22 Dec 2022 21:35:59 +0100

kdevelop-php (22.04.1-1) unstable; urgency=medium

  * New upstream release.
  * Update Vcs-* fields.
  * Bump Standards-Version to 4.6.1, no changes required.
  * CI: enable again the blhc job.
  * kdevelop-php is now part of the KDE release service, so:
    - switch watch file to release-service locations
    - import release-service GPG signing key
  * Update the build dependencies according to the upstream build system:
    - bump cmake to 3.16
    - bump Qt packages to 5.15.0
    - bump KF packages to 5.78.0
    - bump kdevelop-dev to 22.04.1
  * Bump KDEV_PLUGIN_VERSION to 36, according to upstream.
  * Modernize building:
    - add the dh-sequence-kf5 build dependency to use the kf5 addon
      automatically, removing pkg-kde-tools
    - drop the manually specified kf5 addon for dh
  * Remove the explicit as-needed linking, as it is done by binutils now.
  * Remove inactive Uploaders, adding myself as one to avoid leaving the source
    with no human maintainers.

 -- Pino Toscano <pino@debian.org>  Fri, 27 May 2022 11:02:47 +0200

kdevelop-php (5.6.2-2) unstable; urgency=medium

  * Team upload.
  * Use the debhelper 13 replacement features in install files:
    - switch from ${KDEV_PLUGIN_VERSION} to ${env:KDEV_PLUGIN_VERSION}
    - remove the dh-exec build dependency, no more needed
    - remove the lintian override for the old method
  * Bump Standards-Version to 4.6.0, no changes required.
  * CI: disable the blhc job.
  * Use the ${misc:Pre-Depends} substvar in kdevelop-php.

 -- Pino Toscano <pino@debian.org>  Sat, 25 Sep 2021 07:03:28 +0200

kdevelop-php (5.6.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.6.2, as indicated by the
    upstream build system.
  * Use execute_after_dh_auto_install to avoid invoking dh_auto_install
    manually.

 -- Pino Toscano <pino@debian.org>  Wed, 03 Feb 2021 10:22:37 +0100

kdevelop-php (5.6.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.6.1, as indicated by the
    upstream build system.
  * Bump Standards-Version to 4.5.1, no changes required.
  * Switch the watch file to version 4, no changes required.

 -- Pino Toscano <pino@debian.org>  Wed, 09 Dec 2020 10:15:06 +0100

kdevelop-php (5.6.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.6.0, as indicated by the
    upstream build system.
  * Bump KDEV_PLUGIN_VERSION to 34, according to upstream.
  * Bump the debhelper compatibility to 13:
    - switch the debhelper-compat build dependency to 13
    - stop passing --fail-missing to dh_missing, as it is the default behaviour
  * Update lintian overrides.

 -- Pino Toscano <pino@debian.org>  Fri, 11 Sep 2020 16:45:00 +0200

kdevelop-php (5.5.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Set Rules-Requires-Root: no.
  * Bump the kdevelop-dev build dependency to 5.5.2, as indicated by the
    upstream build system.
  * Fix typo in the 5.5.1-1 changelog stanza.

 -- Pino Toscano <pino@debian.org>  Thu, 04 Jun 2020 12:11:52 +0200

kdevelop-php (5.5.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.5.1, as indicated by the
    upstream build system.

 -- Pino Toscano <pino@debian.org>  Sun, 10 May 2020 07:31:44 +0200

kdevelop-php (5.5.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump the debhelper compatibility to 12:
    - switch the debhelper build dependency to debhelper-compat 12
    - remove debian/compat
  * Enable all the reprotest variations in the salsa CI.
  * Bump the kdevelop-dev build dependency to 5.5.0, as indicated by the
    upstream build system.
  * Use an environment variable for the version of plugins, to minimize changes
    to kdevelop-php.install
    - add the dh-exec build dependency
    - override the lintian info on that
  * Bump Standards-Version to 4.5.0, no changes required.
  * Remove breaks/replaces for versions older than two Debian stable releases.

 -- Pino Toscano <pino@debian.org>  Thu, 13 Feb 2020 21:36:37 +0100

kdevelop-php (5.4.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.4.6, as indicated by the
    upstream build system.

 -- Pino Toscano <pino@debian.org>  Thu, 09 Jan 2020 08:27:59 +0100

kdevelop-php (5.4.5-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.4.5, as indicated by the
    upstream build system.

 -- Pino Toscano <pino@debian.org>  Mon, 02 Dec 2019 22:37:58 +0100

kdevelop-php (5.4.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.4.4, as indicated by the
    upstream build system.
  * Bump Standards-Version to 4.4.1, no changes required.
  * Add the configuration for the CI on salsa.

 -- Pino Toscano <pino@debian.org>  Tue, 05 Nov 2019 08:29:06 +0100

kdevelop-php (5.4.2-2) unstable; urgency=medium

  * Team upload.
  * Depend on the newer ecm
  * Move qdebug files

 -- Maximiliano Curia <maxy@debian.org>  Sat, 21 Sep 2019 08:01:33 -0700

kdevelop-php (5.4.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.4.2, as indicated by the
    upstream build system.

 -- Pino Toscano <pino@debian.org>  Sat, 14 Sep 2019 20:15:58 +0200

kdevelop-php (5.4.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.4.1, as indicated by the
    upstream build system.

 -- Pino Toscano <pino@debian.org>  Fri, 16 Aug 2019 18:10:59 +0200

kdevelop-php (5.4.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update the build dependencies according to the upstream build system:
    - bump the Qt packages to >= 5.7.0
    - bump the KF packages to >= 5.28.0
    - bump kdevelop-dev to >= 5.4.0
    - explicitly add gettext
  * Update install files.

 -- Pino Toscano <pino@debian.org>  Sun, 11 Aug 2019 10:08:58 +0200

kdevelop-php (5.3.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.3.3, as indicated by the
    upstream build system.
  * Switch from --list-missing to --fail-missing for dh_missing; everything is
    installed already.

 -- Pino Toscano <pino@debian.org>  Sat, 20 Jul 2019 07:05:43 +0200

kdevelop-php (5.3.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.3.2, as indicated by the
    upstream build system.
  * Bump Standards-Version to 4.4.0, no changes required.
  * Drop the migration from kdevelop-php-dbg, no more needed after two Debian
    stable releases.

 -- Pino Toscano <pino@debian.org>  Tue, 09 Jul 2019 08:13:35 +0200

kdevelop-php (5.3.1-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Pino Toscano <pino@debian.org>  Thu, 20 Dec 2018 22:35:37 +0100

kdevelop-php (5.3.1-1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.3.1, as indicated by the
    upstream build system.

 -- Pino Toscano <pino@debian.org>  Tue, 18 Dec 2018 21:02:59 +0100

kdevelop-php (5.3.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.3.0, as indicated by the
    upstream build system.
  * Update install files.
  * Make sure to not ship the development files, not needed for now.
  * Bump Standards-Version to 4.2.1, no changes required.

 -- Pino Toscano <pino@debian.org>  Sun, 18 Nov 2018 07:55:48 +0100

kdevelop-php (5.2.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.2.4, as indicated by the
    upstream build system.
  * Bump Standards-Version to 4.2.0, no changes required.

 -- Pino Toscano <pino@debian.org>  Tue, 21 Aug 2018 22:31:52 +0200

kdevelop-php (5.2.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Switch Vcs-* fields to salsa.debian.org.
  * Bump the kdevelop-dev build dependency to 5.2.3, as indicated by the
    upstream build system.
  * Bump the debhelper compatibility to 11:
    - bump the debhelper build dependency to 11~
    - bump compat to 11
    - remove --parallel for dh, as now done by default
  * Update install files.
  * Bump Standards-Version to 4.1.4, no changes required.

 -- Pino Toscano <pino@debian.org>  Tue, 22 May 2018 07:48:47 +0200

kdevelop-php (5.2.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.2.1, as indicated by the
    upstream build system.

 -- Pino Toscano <pino@debian.org>  Tue, 28 Nov 2017 07:20:27 +0100

kdevelop-php (5.2.0-2) unstable; urgency=medium

  * Team upload.

  [ Maximiliano Curia ]
  * Update uploaders list as requested by MIA team (Closes: #879335)

 -- Pino Toscano <pino@debian.org>  Wed, 22 Nov 2017 19:40:43 +0100

kdevelop-php (5.2.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Switch the kdevplatform-dev build dependency to kdevelop-dev, following
    the kdevplatform merge in kdevelop.
  * Change other build dependencies according to the upstream build system:
    - bump cmake to >= 3.0
    - bump qtbase-dev to >= 5.5.0
    - bump ECM and Frameworks to >= 5.15.0
    - drop libqt5webkit5-dev, no more required
  * Update install files.
  * Switch the watch file to https.
  * Switch Homepage to https.
  * Bump Standards-Version to 4.1.1, no changes required.
  * Turn the versioned kdevelop dependency into a simple recommend, as it
    should be enough.

 -- Pino Toscano <pino@debian.org>  Fri, 17 Nov 2017 14:25:18 +0100

kdevelop-php (5.1.2-2) unstable; urgency=medium

  * Team upload.
  * Drop the transitional packages kdevelop-php-docs & kdevelop-php-docs-l10n.
  * Bump the kdevelop dependency to 5.1.2.
  * Drop the breaks/replaces in kdevelop-php-l10n against the old
    kdevelop-php-l10n-* packages, since they are long gone.

 -- Pino Toscano <pino@debian.org>  Thu, 14 Sep 2017 07:47:28 +0200

kdevelop-php (5.1.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.1.0, no changes required.
  * Bump the kdevplatform-dev build dependency to 5.1.2, as indicated by the
    upstream build system.

 -- Pino Toscano <pino@debian.org>  Tue, 29 Aug 2017 21:13:13 +0200

kdevelop-php (5.1.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump the kdevplatform-dev build dependency to 5.1.1, as indicated by the
    upstream build system.
  * Bump the kdevelop dependency to 5.1.1.
  * Update install files.
  * Bump Standards-Version to 4.0.0, no changes required.

 -- Pino Toscano <pino@debian.org>  Thu, 06 Jul 2017 12:06:16 +0200

kdevelop-php (5.0.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    - Bump kdevplatform build dependency version.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sat, 07 Jan 2017 17:38:00 -0300

kdevelop-php (5.0.1-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Pino Toscano <pino@debian.org>  Tue, 20 Sep 2016 23:06:22 +0200

kdevelop-php (5.0.1-1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump the kdevplatform-dev build dependency to >= 5.0.1.
  * Bump kdevelop dependency in kdevelop-php to >= 5.0.1.

 -- Pino Toscano <pino@debian.org>  Tue, 20 Sep 2016 07:12:54 +0200

kdevelop-php (5.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Update/simplify watch file.
  * Update the build dependencies following the port to Frameworks:
    - add extra-cmake-modules, qtbase5-dev, libqt5webkit5-dev,
      libkf5archive-dev, libkf5i18n-dev, libkf5itemmodels-dev,
      libkf5kcmutils-dev, libkf5texteditor-dev, and libkf5threadweaver-dev
  * Bump build dependencies according to the upstream build system:
    - bump cmake to >= 2.8.12
    - bump kdevelop-pg-qt to >= 2.0
    - bump kdevplatform-dev to >= 5.0
  * Use the right dh addon:
    - switch from kde to kf5 dh addon
    - bump the pkg-kde-tools build dependency to >= 0.15.16
  * Integrate kdevelop-php-docs into kdevelop-php: the -docs plugin is now
    provided as part of this source, so we can provide it from here; also,
    since the overhead of the -docs plugin and its translations is small,
    install them in kdevelop-php (and kdevelop-php-l10n)
    - provide kdevelop-php-docs and kdevelop-php-docs-l10n as transitional
      packages
    - make kdevelop-php-l10n break/replace kdevelop-php-docs-l10n < 5.0
  * Update install files.
  * Disable building of unit tests, as long as they are not run.
  * Bump kdevelop dependency in kdevelop-php to >= 5.0.
  * Update lintian overrides.

 -- Pino Toscano <pino@debian.org>  Wed, 31 Aug 2016 20:12:45 +0200

kdevelop-php (1.7.3-3) unstable; urgency=medium

  * Team upload.
  * Remove kdevelop-php-dbg in favour of the -dbgsym packages.

 -- Pino Toscano <pino@debian.org>  Mon, 06 Jun 2016 22:42:57 +0200

kdevelop-php (1.7.3-2) unstable; urgency=medium

  * Team upload to unstable.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Thu, 26 May 2016 20:27:46 -0300

kdevelop-php (1.7.3-1) experimental; urgency=medium

  * Team upload.
  * New upstream release (Closes: #818521).
    - Bump kdevplatform-dev to match the latest release.
  * Improve uscan to search for different compression methods in tarballs
    and to repack the tarball with xz compression if necessary.
  * Bump Standards-Version to 3.9.8, no changes required.
  * Update Vcs-[Browser Git] to their https versions.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Thu, 05 May 2016 22:44:33 -0300

kdevelop-php (1.7.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update watch file.
  * Bump kdevplatform-dev build dependency to >= 1.7.2.
  * Bump Standards-Version to 3.9.6, no changes required.

 -- Pino Toscano <pino@debian.org>  Thu, 21 Jan 2016 22:31:16 +0100

kdevelop-php (1.7.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

  [ Andreas Cord-Landwehr ]
  * Bump kdevplatform-dev build dependency to 1.7.0.
  * Bump kdevelop runtime dependency to 4.7.0.

  [ Pino Toscano ]
  * Prepared for upstream release 1.6.0.

 -- Pino Toscano <pino@debian.org>  Tue, 23 Sep 2014 23:10:18 +0200

kdevelop-php (1.5.2-1) unstable; urgency=low

  * Team upload.
  * New upstream release.

  [ Andreas Cord-Landwehr ]
  * Bump kdevplatform-dev build dependency to 1.5.2.
  * Bump kdevelop runtime dependency to 4.5.2.
  * Bump Standards-Version to 3.9.5: no changes needed.
  * Update debian/watch file to track xz compressed tarballs.

 -- Pino Toscano <pino@debian.org>  Sat, 18 Jan 2014 10:25:27 +0100

kdevelop-php (1.5.1-1) unstable; urgency=low

  * Team upload.
  * New upstream release.

  [ Andreas Cord-Landwehr ]
  * Bump kdevplatform-dev build dependency to 1.5.1.
  * Bump Standards-Version to 3.9.4: no changes needed.
  * Update kdevelop-php.install.

  [ Pino Toscano ]
  * Add watch file.
  * Fix Vcs-* headers.

 -- Pino Toscano <pino@debian.org>  Fri, 06 Sep 2013 22:03:28 +0200

kdevelop-php (1.3.1-2) unstable; urgency=low

  * Team upload. Upload to unstable.
  * Link with with as-needed.
  * Remove the docs file, as the upstream TODO is not useful.
  * Small touches to the descriptions.

 -- Pino Toscano <pino@debian.org>  Sun, 20 May 2012 12:30:23 +0200

kdevelop-php (1.3.1-1) experimental; urgency=low

  * Team upload.
  * New upstream release:
    - fixes ftbfs with GCC 4.7. (Closes: #667222)

  [ Andreas Cord-Landwehr ]
  * Bump kdevelop-pg-qt build dependency.
  * Update debian/copyright file.
  * Bump kdevplatform dependency.
  * Bump Standards-Version to 3.9.3: no changes needed.

  [ Felix Geyer ]
  * Mark kdevelop-php as enhancing the kdevelop package.
  * Add ${misc:Depends} to kdevelop-php-dbg and kdevelop-php-l10n.
  * Drop dependency from kdevelop-php-dbg on libc6 [amd64], it's already pulled
    in by kdelibs5-dbg.

  [ Pino Toscano ]
  * Do not pass CMAKE_MODULE_PATH to cmake, since newer kdevelop-pg-qt installs
    cmake configuration files in the proper place.
  * Switch to debhelper compat v9:
    - bump compat to 9
    - bump debhelper build dependency to 9

 -- Pino Toscano <pino@debian.org>  Fri, 18 May 2012 22:36:41 +0200

kdevelop-php (1.2.2-1) unstable; urgency=low

  * New upstream release.

  [ Andreas Cord-Landwehr ]
  * Update debian/control
    - bump kdevplatform-dev build dependency version.
    - removed French (fr) localization package.
    - removed Galician (gl) localization package.
  * Merged all localization packages into one:
    - add Breaks to all previous localization packages.
    - add Replaces to all previous localization packages.
    - contains: Catalan (ca), Southern Catalan (Valencian), Danish (da),
      British English (en_GB), Spanish (es), Estonian (et),
      Italian (it), Low Saxon (nds), Dutch (nl), Portugese (pt),
      Brazilian Portugese (pt_BR), Swedish (sv), Thai (th),
      Ukrainian (uk), Chinese Simplified (zh_CN), Chinese Traditional (zh_TW).
  * Add package with debug symbols for KDevelop PHP plugin. (Closes: #593637)
  * Add myself to Uploaders.

  [ Modestas Vainius ]
  * Switch Vcs-* fields to the new repository at git.debian.org.
  * Bump Standards-Version to 3.9.2: no changes needed.
  * Build depend on kdevplatform-dev and kdevelop-pg-qt.
  * Pass CMAKE_MODULE_PATH=/usr/share/cmake/modules/ to cmake as it does not
    find kdevelop-pg-qt otherwise.
  * Update install files.
  * Add kdevelop-php-l10n-de package.
  * Drop kdevelop-php-l10n-fr and kdevelop-php-l10n-gl packages. Not included
    in this release.
  * Add myself to Uploaders.
  * Add ${misc:Depends} to l10n packages.

 -- Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>  Sat, 04 Jun 2011 13:41:07 +0300

kdevelop-php (1.0.1-1) unstable; urgency=low

  * New upstream release.
  * Add localization packages.

 -- Fathi Boudra <fabo@debian.org>  Fri, 23 Jul 2010 23:45:07 +0300

kdevelop-php (1.0.0-1) unstable; urgency=low

  * New upstream release - upload to unstable.

 -- Fathi Boudra <fabo@debian.org>  Tue, 18 May 2010 12:21:48 +0300

kdevelop-php (1.0.0~rc2-1) experimental; urgency=low

  * New upstream release.
  * Drop patch to bump plugin version - stolen upstream.

 -- Fathi Boudra <fabo@debian.org>  Sat, 17 Apr 2010 15:39:37 +0300

kdevelop-php (1.0.0~rc1-2) experimental; urgency=low

  * Add patch to bump plugin version. (Closes: #576674)

 -- Fathi Boudra <fabo@debian.org>  Tue, 13 Apr 2010 14:53:36 +0300

kdevelop-php (1.0.0~rc1-1) experimental; urgency=low

  * New upstream release.

 -- Fathi Boudra <fabo@debian.org>  Sun, 04 Apr 2010 11:55:33 +0200

kdevelop-php (1.0.0~beta4-1) experimental; urgency=low

  * New upstream release. (Closes: #573811)

 -- Fathi Boudra <fabo@debian.org>  Sun, 14 Mar 2010 10:19:34 +0100

kdevelop-php (1.0.0~beta3-1) experimental; urgency=low

  * New upstream release.
  * Remove 01_kdevelop-plugin_php_branch_pull_r1076222.diff - stolen upstream.
  * Update debian/control:
    - bump build dependencies (debhelper, pkg-kde-tools, kdevplatform-dev).
    - bump Standards-Version to 3.8.4 (no changes needed).
  * Update debian/rules: enable parallel build (pass --parallel option to dh).

 -- Fathi Boudra <fabo@debian.org>  Wed, 17 Feb 2010 22:20:31 +0100

kdevelop-php (1.0.0~beta2-1) experimental; urgency=low

  * Initial release (Closes: #565680)

 -- Fathi Boudra <fabo@debian.org>  Sun, 17 Jan 2010 17:10:22 +0100
